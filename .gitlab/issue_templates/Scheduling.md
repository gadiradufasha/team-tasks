<!--

Set issue title as "Scheduling issue for the MAJOR.MINOR release" where MAJOR.MINOR is the GitLab version.

This template is used to create a release's scheduling issue. The scheduling issue
is used to raise visibility to the team on what issues are being looked at as
candidates for scheduling, and provides a place for them to offer up scheduling suggestions.

Engineering manager and Product manager are responsible for scheduling
work that is a direct responsibility of the Distribution team.

Typically the Engineering manager is the one to create this issue as part of their scheduling workflow

Change milestone_title=MAJOR.MINOR

-->

### Product Outlook
<!--

The product manager should provide a prioritized list of major themes for the release

-->
1. .. @dorrino

### Deliverable Board
Issues on this board have already been reviewed and scheduled for the upcoming release. Each column represents a priority level. The highest ranked issues for each priority level are at the top of each column.

- [Deliverable board](https://gitlab.com/groups/gitlab-org/-/boards/2415614?scope=all&utf8=%E2%9C%93&label_name[]=Deliverable&label_name[]=group%3A%3Adistribution&milestone_title=MAJOR.MINOR)

### For Scheduling board
Used with continuous scheduling process to provide potential candidates with a preferred milestone, but before assigning as ~"Deliverable".
Board includes ~"group::distribution" ~"For Scheduling" issues across gitlab-org projects.

- [For Scheduling board](https://gitlab.com/groups/gitlab-org/-/boards/1282075?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=For%20Scheduling&label_name[]=group%3A%3Adistribution&milestone_title=MAJOR.MINOR)

### Maintenance (formerly Tech Debt) board
Prioritized maintenance issues should be scheduled each release to prevent it from growing unchecked.
Board includes ~"group::distribution" ~"type::maintenance" issues across gitlab-org projects.

- [Maintenance board](https://gitlab.com/groups/gitlab-org/-/boards/2361096?label_name[]=group%3A%3Adistribution&label_name[]=type%3A%3Amaintenance)

### Bugs

Prioritized bugs to be scheduled from input with Quality, Product, Engineering leads.
Board includes ~"group::distribution" ~"type::bug" issues across gitlab-org projects.

- [Bug board](https://gitlab.com/groups/gitlab-org/-/boards/4479153?label_name[]=group%3A%3Adistribution&label_name[]=type%3A%3Abug)

### Sub-Team Boards
Shows issues broken out by ~"Distribution:Deploy" and ~"Distribution:Build" team labels, to ensure we don't have ~"group::distribution" labels missing their sub-team.

- [For Scheduling team board](https://gitlab.com/groups/gitlab-org/-/boards/3748677?label_name[]=For%20Scheduling&label_name[]=group%3A%3Adistribution)
- [Deliverables team board](https://gitlab.com/groups/gitlab-org/-/boards/3668965?label_name[]=Deliverable&label_name[]=group%3A%3Adistribution)

### Ongoing Epics

Some epics are used to collect a common series of issues that we are slowly working through release-over-release. This means we typically try to ensure one task from the epic is in each milestone. We are currently marking these with the ~workflow::scheduling label.

- [Ongoing Epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=group::distribution&label_name[]=workflow::scheduling)

### Comments
The team is encouraged to add potential candidates and contextual comments to this issue. These are reviewed weekly with PM, EM and Staff engineers to determine impact and priority.

/cc @gitlab-org/distribution

/label ~"devops::systems" ~"section::enablement" ~"group::distribution" ~"Planning Issue"
